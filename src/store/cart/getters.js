export const getters = {
  getCart(state) {
    return Object
      .values(state.shoppingCart)
      .filter(item => item);
  },
  getCartCount(state) {
    return Object
      .values(state.shoppingCart)
      .filter(item => item)
      .reduce((total, item) => { total += item.quantity; return total; }, 0);
  }
};

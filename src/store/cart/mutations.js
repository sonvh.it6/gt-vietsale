export const ADD_TO_CART = 'ADD_TO_CART';
export const REMOVE_CART_ITEM = 'REMOVE_CART_ITEM';
export const INCREASE_CART_QUANTITY = 'INCREASE_CART_QUANTITY';
export const DECREASE_CART_QUANTITY = 'DECREASE_CART_QUANTITY';
export const CLEAR_CART = 'CLEAR_CART';

export const mutations = {
  [ADD_TO_CART](state, { item, quantity }) {
    const shoppingCart = Object.assign({}, state.shoppingCart);

    if (state.shoppingCart[item.id]) {
      shoppingCart[item.id].quantity += quantity;
    } else {
      shoppingCart[item.id] = { ...item, quantity };
    }

    state.shoppingCart = shoppingCart;
  },
  [REMOVE_CART_ITEM](state, cartId) {
    const shoppingCart = Object.assign({}, state.shoppingCart);
    delete shoppingCart[cartId];
    state.shoppingCart = shoppingCart;
  },
  [INCREASE_CART_QUANTITY](state, cartId) {
    const shoppingCart = Object.assign({}, state.shoppingCart);
    shoppingCart[cartId].quantity += 1;
    state.shoppingCart = shoppingCart;
  },
  [DECREASE_CART_QUANTITY](state, cartId) {
    const shoppingCart = Object.assign({}, state.shoppingCart);
    if (shoppingCart[cartId].quantity > 1) {
      shoppingCart[cartId].quantity -= 1;
      state.shoppingCart = shoppingCart;
    }
  },
  [CLEAR_CART](state) {
    state.shoppingCart = {};
  }
};

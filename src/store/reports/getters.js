export const getters = {
  getOrderReports(state) {
    return state.orderReports.map((order, index) => ({ ...order, id: index + 1 }));
  }
};

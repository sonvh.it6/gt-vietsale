import { SET_CITIES } from './mutations';

export const actions = {
  async getCities({ commit, state }) {
    if (state.cities.length === 0) {
      const result = await this._vm.$apollo.query(`
        {
          getCities {
            name
            districts {
              name
              wards {
                name
              }
            }
          }
        }
      `);

      if (result && result.getCities) {
        commit(SET_CITIES, result.getCities);
      }
    }
  },
};

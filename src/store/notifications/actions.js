import _get from 'lodash/get';
import {
  SET_NOTIFICATIONS,
  ADD_NOTIFICATION,
  UPDATE_NOTIFICATION,
  DELETE_NOTIFICATION,
  SET_FCM_REGISTRATION_TOKEN,
  MODIFY_NOTIFICATION,
  MARK_READ,
  SET_USER_NOTIFICATIONS,
} from './mutations';
import { USER_ROLE } from '../../commons/enums';

let notificationSubscription;

export const actions = {
  async getUserNotifications({ commit }, userId = '') {
    const result = await this._vm.$apollo.query(`
      {
        userGetNotifications(userId: ${userId}) {
          id
          title
          content
          status
          createdAt
        }
      }
    `);

    if (result && result.userGetNotifications) {
      commit(SET_USER_NOTIFICATIONS, result.userGetNotifications);
    }
  },
  async getNotifications({ commit }, options = {}) {
    const {
      filter = '',
      page = 0,
      limit = 10,
      orderBy = '',
      order = '',
    } = options;

    const result = await this._vm.$apollo.query(`
      {
        getNotifications(
          pagination: {
            page: ${page},
            limit: ${limit},
            orderBy: "${orderBy}",
            order: "${order}",
          },
          filters: {
            generic: "${filter}",
          }
        ) {
          notifications {
            id
            title
            content
            createdAt
          }
          page
          total
        }
      }
    `);

    if (result && result.getNotifications) {
      commit(SET_NOTIFICATIONS, result.getNotifications);
    }
  },
  createNotification({ commit }, { title = '', content = '', toUsers = [] }) {
    return new Promise(async (resolve, reject) => {
      const result = await this._vm.$apollo.mutation(`
        mutation {
          createNotification(input: {
            title: "${title}",
            content: "${content}",
            toUsers: [${toUsers.map((u) => u.value).join(',')}]
          }) {
            id
            title
            content
            createdAt
          }
        }
      `);

      if (result && result.createNotification) {
        commit(ADD_NOTIFICATION, result.createNotification);
        this._vm.$notify.success('Tạo thông báo mới thành công.');
        return resolve(result.createNotification);
      }
      return reject();
    });
  },
  updateNotification(
    { commit },
    { id = '', title = '', content = '', toUsers = [] },
  ) {
    return new Promise(async (resolve, reject) => {
      const result = await this._vm.$apollo.mutation(`
        mutation {
          updateNotification(input: {
            id: ${id},
            title: "${title}",
            content: "${content}",
            toUsers: [${toUsers.map((u) => u.value).join(',')}]
          }) {
            id
            title
            content
            createdAt
          }
        }
      `);

      if (result && result.updateNotification) {
        commit(UPDATE_NOTIFICATION, result.updateNotification);
        this._vm.$notify.success('Chỉnh sửa thông báo thành công.');
        return resolve(result.updateNotification);
      }
      return reject();
    });
  },
  async deleteNotification({ commit }, id) {
    const result = await this._vm.$apollo.mutation(`
      mutation {
        deleteNotification(id: ${id})
      }
    `);

    if (result && result.deleteNotification) {
      commit(DELETE_NOTIFICATION, id);
      this._vm.$notify.success('Xoá thông báo thành công.');
    }
  },
  updateFcmToken({ rootState, state, commit }, token = null) {
    const fcmToken = token || state.fcmToken;
    commit(SET_FCM_REGISTRATION_TOKEN, fcmToken);

    const userId = _get(rootState, 'auth.user.id', null);
    const userRole = _get(rootState, 'auth.user.role', null);

    if (userId && fcmToken && userRole === USER_ROLE.USER) {
      this._vm.$apollo.mutation(`
        mutation {
          updateFcmToken(input: {userId: ${userId}, token: "${fcmToken}"})
        }
      `);
    }
  },
  subscribeNotifications({ commit, rootState }) {
    const userId = _get(rootState, 'auth.user.id', null);

    if (notificationSubscription) {
      notificationSubscription.unsubscribe();
    }

    if (userId !== null) {
      notificationSubscription = this._vm.$apollo
        .subscribe(
          `
            subscription {
              pushNotification(userId: ${userId}) {
                id
                title
                content
                createdAt
              }
            }
          `,
        )
        .subscribe({
          next({ data }) {
            if (data.pushNotification) {
              commit(MODIFY_NOTIFICATION, {
                ...data.pushNotification,
                status: 'UNREAD',
              });
            }
          },
        });
    }
  },
  unsubscribeNotifications() {
    if (notificationSubscription) {
      notificationSubscription.unsubscribe();
    }
  },
  async markRead({ commit }, id = '') {
    const result = await this._vm.$apollo.mutation(`
      mutation {
        markRead(${id ? `id: ${id}` : ''})
      }
    `);

    if (result && result.markRead) {
      commit(MARK_READ, id);
    }
  },
};

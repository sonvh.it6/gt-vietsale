import { RESET_WHEN_LOGOUT, SET_USER, SET_TOKEN } from './mutations';
import router from '~/router';
import { USER_ROLE } from '../../commons/enums';

export const actions = {
  async login({ commit }, { id = '', password = '', role = USER_ROLE.USER }) {
    const result = await this._vm.$apollo.query(`
      {
        login(input: { id: ${id}, password: "${password}", role: ${role} })
      }
    `);

    if (result) {
      localStorage.removeItem('token');
      localStorage.removeItem('admin_token');
      commit(SET_TOKEN, result.login);

      if (role === USER_ROLE.USER) {
        await localStorage.setItem('token', result.login);
        router.push('/');
      } else {
        await localStorage.setItem('admin_token', result.login);
        router.push('/admin');
      }
    }
  },
  async register(_, user = {}) {
    if (!user.isAccept) {
      return this._vm.$notify.error(
        'Vui lòng chấp nhận với những điều khoản và điều kiện của chúng tôi.',
      );
    }

    delete user.isAccept;

    if (user.gender) {
      user.gender = user.gender ? user.gender.value : '';
    }
    const inputText = Object.keys(user).map((key) => {
      switch (key) {
        case 'gender':
        case 'userRetail':
          return `${key}: ${user[key]}`;
        case 'media':
          return `medias: [${
            user.medias
              ? user.medias.map((media) => `"${media}"`).join(',')
              : ''
          }]`;
        default:
          return `${key}: "${user[key] || ''}"`;
      }
    });

    const result = await this._vm.$apollo.mutation(`
      mutation {
        register(input: { ${inputText} }) {
          id
        }
      }
    `);

    if (result && result.register) {
      return result.register.id;
    }

    return null;
  },
  async getAuthUser({ commit, dispatch }) {
    const result = await this._vm.$apollo.query(`
      {
        getAuthUser {
          id
          fullName
          phoneNumber
          email
          gender
          role
          userRetail
          city
          district
          ward
          address
          nationalId
          passport
          otherInformation
          companyAddress
          companyPhone
          bankOwner
          bankNumber
          bankBranch
          referral {
            id
            fullName
            phoneNumber
            fullAddress
          }
        }
      }
    `);
    if (result && result.getAuthUser) {
      commit(SET_USER, result.getAuthUser);
      dispatch('notifications/updateFcmToken', null, { root: true });
    }
  },
  async updateMyInfo({ commit }, user = {}) {
    if (user.gender) {
      user.gender = user.gender ? user.gender.value : '';
    }
    const inputText = Object.keys(user).map((key) => {
      switch (key) {
        case 'gender':
          return `${key}: ${user[key]}`;
        default:
          return `${key}: "${user[key] || ''}"`;
      }
    });

    const result = await this._vm.$apollo.mutation(`
      mutation {
        updateMyInfo(input: { ${inputText} }) {
          id
          fullName
          phoneNumber
          email
          gender
          role
          userRetail
          city
          district
          ward
          address
          nationalId
          passport
          otherInformation
          companyAddress
          companyPhone
          bankOwner
          bankNumber
          bankBranch
          referral {
            id
            fullName
            phoneNumber
            fullAddress
          }
        }
      }
    `);

    if (result && result.updateMyInfo) {
      commit(SET_USER, result.updateMyInfo);
      this._vm.$notify.success('Cập nhật thông tin cá nhân thành công!');
    }

    return result;
  },
  async logout({ commit }, role = USER_ROLE.USER) {
    this._vm.$apollo.mutation(`
      mutation {
        logout
      }
    `);

    commit(RESET_WHEN_LOGOUT);

    const intervalId = window.setInterval(() => {}, 9999); // Get a reference to the last
    // interval + 1
    for (let i = 1; i < intervalId; i += 1) window.clearInterval(i);

    await Promise.all([
      localStorage.removeItem('token'),
      localStorage.removeItem('admin_token'),
    ]);

    if (role === USER_ROLE.USER) {
      router.push('/login');
    } else {
      router.push('/admin/login');
    }
  },
  async forgetPassword(_, email = '') {
    if (email) {
      const result = await this._vm.$apollo.mutation(`
        mutation {
          forgetPassword(email: "${email}")
        }
      `);

      if (result && result.forgetPassword) {
        this._vm.$notify.success(
          'Đã gửi mật khẩu mới, vui lòng kiểm tra hộp thư của bạn.',
        );
        return true;
      }
      return false;
    }
    return false;
  },
};

export const getters = {
  getProductCategories(state) {
    return state.productCategories.map(({ media, ...data }) => ({
      ...data,
      media: media ? media.path : '',
      mediaArr: media ? [media.path] : [],
    }));
  },
  getProductCategoryById(state) {
    return id => state.productCategories.find(category => category.id === id);
  }
};

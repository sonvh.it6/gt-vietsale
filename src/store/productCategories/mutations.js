export const SET_PRODUCT_CATEGORIES = 'SET_PRODUCT_CATEGORIES';
export const MODIFY_PRODUCT_CATEGORY = 'MODIFY_PRODUCT_CATEGORY';
export const DELETE_PRODUCT_CATEGORY = 'DELETE_PRODUCT_CATEGORY';

export const mutations = {
  [SET_PRODUCT_CATEGORIES](state, productCategoryPagination) {
    state.productCategories = productCategoryPagination.categories;
    state.categoryPage = productCategoryPagination.page;
    state.totalCategories = productCategoryPagination.total;
  },
  [MODIFY_PRODUCT_CATEGORY](state, productCategory) {
    const listCategories = [...state.productCategories];
    const idx = listCategories.findIndex(pc => pc.id === productCategory.id);
    if (idx > -1) {
      listCategories[idx] = productCategory;
    } else {
      listCategories.push(productCategory);
    }

    state.productCategories = listCategories;
  },
  [DELETE_PRODUCT_CATEGORY](state, id) {
    state.productCategories = state.productCategories.filter(pc => pc.id !== id);
  }
};

export const SET_PRODUCT_BRANDS = 'SET_PRODUCT_BRANDS';
export const MODIFY_PRODUCT_BRAND = 'MODIFY_PRODUCT_BRAND';
export const DELETE_PRODUCT_BRAND = 'DELETE_PRODUCT_BRAND';

export const mutations = {
  [SET_PRODUCT_BRANDS](state, productBrandPagination) {
    state.productBrands = productBrandPagination.brands;
    state.brandPage = productBrandPagination.page;
    state.totalBrands = productBrandPagination.total;
  },
  [MODIFY_PRODUCT_BRAND](state, productBrand) {
    const listBrands = [...state.productBrands];
    const idx = listBrands.findIndex(pc => pc.id === productBrand.id);
    if (idx > -1) {
      listBrands[idx] = productBrand;
    } else {
      listBrands.push(productBrand);
    }

    state.productBrands = listBrands;
  },
  [DELETE_PRODUCT_BRAND](state, id) {
    state.productBrands = state.productBrands.filter(pc => pc.id !== id);
  }
};

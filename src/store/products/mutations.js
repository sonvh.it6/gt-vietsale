export const SET_PRODUCTS = 'SET_PRODUCTS';
export const MODIFY_PRODUCT = 'MODIFY_PRODUCT';
export const DELETE_PRODUCT = 'DELETE_PRODUCT';

export const mutations = {
  [SET_PRODUCTS](state, productPagination) {
    state.products = productPagination.products;
    state.productPage = productPagination.page;
    state.totalProducts = productPagination.total;
  },
  [MODIFY_PRODUCT](state, product) {
    const listProducts = [...state.products];
    const idx = listProducts.findIndex(p => p.id === product.id);
    if (idx > -1) {
      listProducts[idx] = product;
    } else {
      listProducts.push(product);
    }

    state.products = listProducts;
  },
  [DELETE_PRODUCT](state, productId) {
    state.products = state.products.filter((p) => p.id !== productId);
  },
};

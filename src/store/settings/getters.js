export const getters = {
  getUserRetailFee(state) {
    const contributorShippingFee = state.settings.find(
      (s) => s.settingName === 'contributor_shipping_fee',
    );
    const contributorEducatedShippingFee = state.settings.find(
      (s) => s.settingName === 'contributor_educated_shipping_fee',
    );
    const saleShippingFee = state.settings.find(
      (s) => s.settingName === 'sale_shipping_fee',
    );
    const deliveryShippingFee = state.settings.find(
      (s) => s.settingName === 'delivery_shipping_fee',
    );

    return {
      CONTRIBUTOR: contributorShippingFee
        ? contributorShippingFee.settingValue
        : 0,
      CONTRIBUTOR_EDUCATED: contributorEducatedShippingFee
        ? contributorEducatedShippingFee.settingValue
        : 0,
      SALE: saleShippingFee ? saleShippingFee.settingValue : 0,
      DELIVERY: deliveryShippingFee ? deliveryShippingFee.settingValue : 0,
    };
  },
};

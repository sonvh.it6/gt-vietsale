import {
  SET_USER_PAGINATION,
  VERIFY_USER,
  DELETE_USER,
  UPDATE_USER_RETAIL,
  MODIFY_USER,
} from './mutations';
import { CommonUtils } from '../../utils/commons';

const userQuery = `
  id
  fullName
  phoneNumber
  email
  gender
  city
  district
  ward
  address
  fullAddress
  userRetail
  nationalId
  passport
  otherInformation
  companyAddress
  companyPhone
  bankOwner
  bankNumber
  bankBranch
  status
  createdAt
  medias {
    path
  }
  referral {
    id
    fullName
  }
  validator {
    id
    fullName
  }
  totalSpend
`;

export const actions = {
  async getUsers({ commit }, options = {}) {
    const {
      filter = '',
      page = 0,
      limit = 10,
      orderBy = '',
      order = '',
      referral = '',
    } = options;

    const result = await this._vm.$apollo.query(`
      {
        getUsers(pagination: {
          page: ${page},
          limit: ${limit},
          orderBy: "${orderBy}",
          order: "${order}"
        }, filters: {
          generic: "${filter}",
          ${referral ? `referral: ${referral},` : ''} 
        }) {
          users {
            ${userQuery}
          }
          page
          total
        }
      }
    `);

    if (result && result.getUsers) {
      commit(SET_USER_PAGINATION, result.getUsers);
    }
  },
  async updateUser({ commit }, user = {}) {
    const inputList = ['password', 'confirmPassword']
      .map((key) => {
        if (user[key]) {
          switch (key) {
            default:
              return `${key}: "${CommonUtils.escapeHtml(user[key]) || ''}"`;
          }
        }
        return null;
      })
      .filter((i) => i !== null);

    const result = await this._vm.$apollo.mutation(`
      mutation {
        updateUser(id: ${user.id}, input: { ${inputList} }) {
          ${userQuery}
        }
      }
    `);

    if (result && result.updateUser) {
      commit(MODIFY_USER, result.updateUser);
      this._vm.$notify.success('Chỉnh sửa người dùng thành công.');
    }
  },
  async verifyUser({ commit }, id) {
    const result = await this._vm.$apollo.mutation(`
      mutation {
        verifyUser(id: ${id}) {
          id
          status
          validator {
            id
            fullName
          }
        }
      }
    `);

    if (result && result.verifyUser) {
      commit(VERIFY_USER, result.verifyUser);
      this._vm.$notify.success('Xác thực thành viên thành công.');
    }
  },
  async confirmEducatedUser({ commit }, id) {
    const result = await this._vm.$apollo.mutation(`
      mutation {
        confirmEducatedUser(id: ${id}) {
          id
          userRetail
        }
      }
    `);

    if (result && result.confirmEducatedUser) {
      commit(UPDATE_USER_RETAIL, result.confirmEducatedUser);
      this._vm.$notify.success('Chứng thực thành viên đã đi học thành công.');
    }
  },
  async getOrderReceivers(_, { city, district, ward }) {
    const result = await this._vm.$apollo.query(`
      {
        getOrderReceivers(city: "${city}", district: "${district}", ward: "${ward}") {
          id
          fullName
          fullAddress
        }
      }
    `);

    if (result && result.getOrderReceivers) {
      return result.getOrderReceivers;
    }

    return [];
  },
  async deleteUser({ commit }, id) {
    const result = await this._vm.$apollo.mutation(`
      mutation {
        deleteUser(id: ${id})
      }
    `);

    if (result && result.deleteUser) {
      commit(DELETE_USER, id);
      this._vm.$notify.success('Xoá người dùng thành công.');
    }
  },
};

export const SET_ORDERS = 'SET_ORDERS';
export const MODIFY_ORDER = 'MODIFY_ORDER';
export const DELETE_ORDER = 'DELETE_ORDER';

export const mutations = {
  [SET_ORDERS](state, orderPagination) {
    state.orders = orderPagination.orders;
    state.orderPage = orderPagination.page;
    state.totalOrders = orderPagination.total;
  },
  [MODIFY_ORDER](state, order) {
    const listOrders = [...state.orders];
    const idx = listOrders.findIndex(o => o.id === order.id);
    if (idx > -1) {
      listOrders[idx] = order;
    } else {
      listOrders.push(order);
    }

    state.orders = listOrders;
  },
  [DELETE_ORDER](state, orderId) {
    state.orders = state.orders.filter((o) => o.id !== orderId);
  },
};

import VuexPersistence from 'vuex-persist';

export default async ({ store }) => {
  new VuexPersistence({
    key: 'vietdistributions',
    modules: ['auth', 'cart', 'notifications', 'resources'],
  }).plugin(store);
};
